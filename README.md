<div align="center">
  <img src="assets/img/logo.png" alt="Logo" width="200">

  ### A web application for promoting and discovering events.
</div>

<br>
<br>
<br>

## About the Project

This project uses the Spring framework with Thymeleaf to build a simple web application. 

Hosts can post events on the platform. Visitors can then express their interest by voting.


## Getting Started

In order to use this software you have to download the [released](https://gitlab.com/lorenzsimon/eventify/-/releases) jar file and move it to your application directory. Please configure a postgreSQL database in the application.properties file. You can then start it by executing the jar file.


## Documentation

For detailed documentation, please refer to the folder `docs`.


## License

Distributed under the GNU GLP License. See `LICENSE` for more information.
